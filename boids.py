import numpy as np
import sys
import pygame
import heapq
import math
import random
from pygame.locals import *

env=(400,500)
pygame.init() #initialize
screen=pygame.display.set_mode(env, 0, 32)  #set_screen(x,y)
screen.fill((255,255,255)) #fill screen
pygame.display.set_caption("boids test")
framerate=10 #
clock=pygame.time.Clock()

agent=["a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t"] # 20agent



class Agent:
    def __init__(self,id,env):
        posi=(random.randint(10,env[0]-1),random.randint(10,env[1]-1)) #initial_position x,y
        self.x=posi[0]
        self.y=posi[1]
        self.neighbors=[] #neightbors agent
        self.acc=3
        self.deg=random.randint(0,360) #agent degree
        self.vision=100   #agent vision
        self.personal=50 #personal space
        self.id=id #agent id
        #self.max_range=180
        self.par_a=5 #align parameter
        self.par_c=5 #cohesion parameter
        self.par_s=5 #separation parameter

    def get_posi(self):
        return (self.x,self.y)

    def re_posi(self):
        self.x=random.randint(10,env[0]-1)
        self.y=random.randint(10,env[1]-1)

    def check_neighbors(self): #search agent
        self.neighbors=[]
        for i in range(0,len(agent)):
            d=(abs(self.x-agent[i].x)+abs(self.y-agent[i].y)) #distance
            if (d < self.vision):
                heapq.heappush(self.neighbors,(d, agent[i].deg, agent[i].x, agent[i].y)) #distance,agent angle,x,y
        heapq.heappop(self.neighbors)

    def walk_agent(self,deg):
        ali=0
        sep=0
        coh=0

        if(math.cos(math.radians(deg))<0):
            self.x=self.x+int(self.acc*math.floor(math.cos(math.radians(deg))))
        else:
            self.x=self.x+int(self.acc*math.ceil(math.cos(math.radians(deg))))
        if(math.sin(math.radians(deg))<0):
            self.y=self.y+int(self.acc*math.floor(math.sin(math.radians(deg))))
        else:
            self.y=self.y+int(self.acc*math.ceil(math.sin(math.radians(deg))))

        print("id={} x={},y={}".format(self.id,self.x,self.y))

        self.check_neighbors()
        print("neightbors {}".format(self.neighbors))
        if(len(self.neighbors)!=0):
            heap=heapq.heappop(self.neighbors)
            if(heap[0]<self.personal):
                print("in a personal")
                sep=self.deg+self.calc_deg(heap[1]+180,self.deg,360)
            coh=self.cohe_boid()
            ali=self.align_boid()
        if(sep+ali+coh!=0):
            self.deg=((sep/(sep+ali+coh))*self.par_s)+((ali/(sep+ali+coh))*self.par_a)+((coh/(sep+ali+coh))*self.par_c)+self.deg
        else:
            self.deg=((sep)*self.par_s)+((ali)*self.par_a)+((coh)*self.par_c)+self.deg

        if(self.deg>360):self.deg=self.deg%360
        print("deg {}".format(self.deg))

    def calc_deg(self,deg1,deg2,period):
        deg=((deg1)-deg2)%period

        if(abs(deg)<=period/2):
            return deg
        else:
            if(deg>0):
                return (-period+deg)
            else:
                return (period+deg)

    def align_boid(self):
        ax = 0
        ay = 0
        degrees = 180 / math.pi
        for i in range (0,len(self.neighbors)):
            ax = ax + int(math.cos(math.radians(self.neighbors[i][1])))
            ay = ay + int(math.sin(math.radians(self.neighbors[i][1])))
        return self.calc_deg(math.atan2(ay,ax)*degrees,self.deg,360)

    def cohe_boid(self):
        cx = 0
        cy = 0
        degrees = 180 / math.pi
        for j in range (0,len(self.neighbors)):
            cx = cx + int(self.calc_deg(self.neighbors[j][2], self.x ,env[0]))
            cy = cy + int(self.calc_deg(self.neighbors[j][3], self.y ,env[1]))
        return self.calc_deg(math.atan2(cy,cx)*degrees,self.deg,360)

for i in range(0,len(agent)):
    agent[i]=Agent(agent[i],env)

def draw_boids(): #draw agents
    screen.fill((255,255,255))
    for i in range(0,len(agent)):
        point=agent[i].get_posi()
        deg=agent[i].deg
        pygame.draw.circle(screen, (255,0,0), point, 5)
        pygame.draw.line(screen,(0,0,0),(point),(int(math.cos(math.radians(deg))*10)+point[0],int(math.sin(math.radians(deg))*10)+point[1]),2)

def main():
    while(1):
        pygame.display.update()
        draw_boids()
        for i in range(0,len(agent)):
            agent[i].walk_agent(agent[i].deg)

            x,y=agent[i].get_posi()
            if(x>=env[0] or x<=0 or y>=env[1] or y<=0):
                agent[i].deg=agent[i].deg+180
                if(agent[i].deg>360):agent[i].deg=agent[i].deg-360
                if(x>=env[0]+10 or x<=0-10 or y>=env[1]+10 or y<=0-10):
                    agent[i].re_posi()
        for event in pygame.event.get(): #terminate
            if event.type==QUIT:
                pygame.quit()
                sys.exit()

        clock.tick(framerate)


if __name__=='__main__':
    main()
